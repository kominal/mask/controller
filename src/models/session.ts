import { Document, model, Schema } from 'mongoose';

export interface Session {
	group: string;
	identifier: string;
	family: number;
	maskIp: string;
	lastActivity: Date;
}

export const SessionDatabase = model<Document & Session>(
	'Session',
	new Schema(
		{
			group: String,
			identifier: String,
			family: Number,
			maskIp: String,
			lastActivity: Date,
		},
		{ minimize: false }
	)
);
