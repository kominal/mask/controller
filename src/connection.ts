import { info, warn } from '@kominal/observer-node-client';
import { Socket } from 'socket.io';
import { compareSessions, markSessionActivity } from './helper/helper';
import { Session } from './models/session';

export const connections: Connection[] = [];

export class Connection {
	public configuredFloatingIps: number[] = [];
	public nodeHostname?: string;
	public sessions: Session[] = [];
	private id: string;
	public ip?: string;

	constructor(public socket: Socket) {
		this.id = this.socket.id;
		info(`New proxy connected.`);
		socket.on('REGISTER', (data) => this.onRegister(data));
		socket.on('SESSIONS', (data) => this.onSessions(data));
		socket.on('disconnect', () => this.onDisconnect());
		socket.on('error', (e) => warn(e));
	}

	async onRegister(data: { nodeHostname: string; ip: string }) {
		this.nodeHostname = data.nodeHostname;
		this.ip = data.ip;
		info(`New proxy has registered with node hostname ${this.nodeHostname} and ip ${this.ip}.`);
		connections.push(this);
	}

	onSessions({ sessions }: { sessions: Session[] }): Promise<void[]> {
		const changedSessions: Session[] = [];

		for (const session of [...this.sessions, ...sessions]) {
			if (this.sessions.filter((s) => compareSessions(s, session)).length === 1) {
				//Session is not present in both past and current sessions => Disconnected or Connected
				changedSessions.push(session);
			}
		}

		this.sessions = sessions;

		return Promise.all(changedSessions.map((s) => markSessionActivity(s)));
	}

	onDisconnect(): Promise<void[]> {
		info(`Lost connection to proxy with node hostname ${this.nodeHostname} and ip ${this.ip}.`);
		const index = connections.findIndex((p) => p.id === this.id);
		if (index > -1) {
			connections.splice(index, 1);
		}

		return Promise.all(this.sessions.map((s) => markSessionActivity(s)));
	}
}
