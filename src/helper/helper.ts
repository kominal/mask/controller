import { Session, SessionDatabase } from '../models/session';

export function compareSessions(session1: Session, sessions2: Session) {
	return (
		session1.maskIp === sessions2.maskIp &&
		session1.group === sessions2.group &&
		session1.identifier === sessions2.identifier &&
		session1.family === sessions2.family
	);
}

export async function markSessionActivity(session: Session) {
	await SessionDatabase.updateOne(
		{
			maskIp: session.maskIp,
			group: session.group,
			identifier: session.identifier,
			family: session.family,
		},
		{ lastActivity: new Date() }
	);
}
